package com.xixiwan.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xixiwan.model.User;
import com.xixiwan.service.HelloService;

@RestController
public class HelloController {

	@Autowired
	private HelloService helloService;

	@GetMapping("/hello")
	public String hello(@RequestParam("name") String name) {
		return helloService.hello(name);
	}

	@PostMapping("/addUser")
	public String addUser(@RequestBody User user) {
		return helloService.addUser(user);
	}

}
