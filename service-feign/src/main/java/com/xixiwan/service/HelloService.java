package com.xixiwan.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.xixiwan.model.User;
import com.xixiwan.service.impl.HelloServiceImpl;

//@FeignClient(name = "service-hello", fallback = HiServiceImpl.class)
@FeignClient(name = "service-hello", fallbackFactory = HelloServiceImpl.class)
public interface HelloService {

	@GetMapping("/hello")
	public String hello(@RequestParam("name") String name);

	@PostMapping("/addUser")
	public String addUser(User user);
}
