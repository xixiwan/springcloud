package com.xixiwan.service.impl;

import org.springframework.stereotype.Component;

import com.xixiwan.model.User;
import com.xixiwan.service.HelloService;

import feign.hystrix.FallbackFactory;

@Component
// public class HelloServiceImpl implements HelloService {
public class HelloServiceImpl implements FallbackFactory<HelloService> {

	@Override
	public HelloService create(Throwable cause) {
		return new HelloService() {
			@Override
			public String hello(String name) {
				return "fallback " + name + "; reason was: " + cause.getMessage();
			}

			@Override
			public String addUser(User user) {
				return "fallback " + user.getUserName() + "; reason was: " + cause.getMessage();
			}
		};
	}

	// @Override
	// public String hello(String name) {
	// return "sorry "+name;
	// }

}
