package com.xixiwan.model;

public class User {
	/** 主键 */
	private String id;

	/** 编号(工号) */
	private String code;

	/** 登录名，用户名 */
	private String userName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}
