package com.xixiwan.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xixiwan.service.HiService;

@RestController
public class HiController {

	@Autowired
	private HiService hiService;

	@GetMapping("/hi")
	public String hi(@RequestParam String name) {
		return hiService.hi(name);
	}

	@GetMapping("/hello")
	public String hello(@RequestParam String name) {
		return hiService.hello(name);
	}

}
