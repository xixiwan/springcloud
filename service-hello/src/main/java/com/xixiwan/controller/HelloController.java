package com.xixiwan.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.xixiwan.model.User;

@RestController
public class HelloController {

	@Value("${server.port}")
	String port;

	@GetMapping("/hello")
	@HystrixCommand(fallbackMethod = "helloError")
	public String hello(@RequestParam("name") String name) {
		return "hello " + name + ",i am from port:" + port;
	}

	public String helloError(String name) {
		return "hello " + name + ",sorry,error!";
	}

	@PostMapping("/addUser")
	@HystrixCommand(fallbackMethod = "addUserError")
	public String addUser(@RequestBody User user) {
		return "addUser " + user.getUserName() + ",i am from port:" + port;
	}

	public String addUserError(User user) {
		return "addUser " + user.getUserName() + ",sorry,error!";
	}
}
