package com.xixiwan.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@RestController
public class HiController {

	@Value("${server.port}")
	String port;

	@GetMapping("/hi")
	@HystrixCommand(fallbackMethod = "hiError")
	public String hi(@RequestParam("name") String name) {
		return "hi " + name + ",i am from port:" + port;
	}

	public String hiError(String name) {
		return "hi," + name + ",sorry,error!";
	}
}
