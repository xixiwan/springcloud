package com.xixiwan.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TopicConfig {

	public static final String TOPIC_EXCHANGE = "xixiwan_topic_exchange";

	public static final String TOPIC_QUEUE_A = "xixiwan_topic.a";

	public static final String TOPIC_QUEUE_B = "xixiwan_topic.a.b";

	public static final String TOPIC_QUEUE_C = "xixiwan_topic.a.b.c";

	public static final String TOPIC_GLOBBING_A = "xixiwan_topic.a.*";

	public static final String TOPIC_GLOBBING_A_B = "xixiwan_topic.a.b.*";

	@Bean
	public TopicExchange topicExchange() {
		return new TopicExchange(TOPIC_EXCHANGE);
	}

	@Bean
	public Queue topicQueueA() {
		return new Queue(TOPIC_QUEUE_A);
	}

	@Bean
	public Queue topicQueueB() {
		return new Queue(TOPIC_QUEUE_B);
	}

	@Bean
	public Queue topicQueueC() {
		return new Queue(TOPIC_QUEUE_C);
	}

	@Bean
	public Binding bindingTopicExchangeA() {
		return BindingBuilder.bind(topicQueueA()).to(topicExchange()).with(TOPIC_GLOBBING_A);
	}

	@Bean
	public Binding bindingTopicExchangeB() {
		return BindingBuilder.bind(topicQueueB()).to(topicExchange()).with(TOPIC_GLOBBING_A);
	}

	@Bean
	public Binding bindingTopicExchangeC() {
		return BindingBuilder.bind(topicQueueC()).to(topicExchange()).with(TOPIC_GLOBBING_A_B);
	}

}
