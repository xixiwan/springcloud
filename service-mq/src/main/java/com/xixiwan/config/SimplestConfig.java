package com.xixiwan.config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SimplestConfig {

	public static final String SIMPLEST_QUEUE = "xixiwan_simplest";

	@Bean
	public Queue Queue() {
		return new Queue(SIMPLEST_QUEUE);
	}

}
