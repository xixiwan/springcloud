package com.xixiwan.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FanoutConfig {

	public static final String FANOUT_EXCHANGE = "xixiwan_fanout_exchange";

	public static final String FANOUT_QUEUE_A = "xixiwan_fanout_a";

	public static final String FANOUT_QUEUE_B = "xixiwan_fanout_b";

	public static final String FANOUT_QUEUE_C = "xixiwan_fanout_c";

	@Bean
	public FanoutExchange fanoutExchange() {
		return new FanoutExchange(FANOUT_EXCHANGE);
	}

	@Bean
	public Queue fanoutQueueA() {
		return new Queue(FANOUT_QUEUE_A);
	}

	@Bean
	public Queue fanoutQueueB() {
		return new Queue(FANOUT_QUEUE_B);
	}

	@Bean
	public Queue fanoutQueueC() {
		return new Queue(FANOUT_QUEUE_C);
	}

	@Bean
	public Binding bindingFanoutExchangeA() {
		return BindingBuilder.bind(fanoutQueueA()).to(fanoutExchange());
	}

	@Bean
	public Binding bindingFanoutExchangeB() {
		return BindingBuilder.bind(fanoutQueueB()).to(fanoutExchange());
	}

	@Bean
	public Binding bindingFanoutExchangeC() {
		return BindingBuilder.bind(fanoutQueueC()).to(fanoutExchange());
	}

}
