package com.xixiwan.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DirectConfig {

	public static final String DIRECT_EXCHANGE = "xixiwan_direct_exchange";

	public static final String DIRECT_QUEUE_A = "xixiwan_direct_a";

	public static final String DIRECT_QUEUE_B = "xixiwan_direct_b";

	public static final String DIRECT_QUEUE_C = "xixiwan_direct_c";

	@Bean
	public DirectExchange directExchange() {
		return new DirectExchange(DIRECT_EXCHANGE);
	}

	@Bean
	public Queue directQueueA() {
		return new Queue(DIRECT_QUEUE_A);
	}

	@Bean
	public Queue directQueueB() {
		return new Queue(DIRECT_QUEUE_B);
	}

	@Bean
	public Queue directQueueC() {
		return new Queue(DIRECT_QUEUE_C);
	}

	@Bean
	public Binding bindingDirectExchangeA() {
		return BindingBuilder.bind(directQueueA()).to(directExchange()).with(DIRECT_QUEUE_A);
	}

	@Bean
	public Binding bindingDirectExchangeB() {
		return BindingBuilder.bind(directQueueB()).to(directExchange()).with(DIRECT_QUEUE_B);
	}

	@Bean
	public Binding bindingDirectExchangeC() {
		return BindingBuilder.bind(directQueueC()).to(directExchange()).with(DIRECT_QUEUE_C);
	}

}
