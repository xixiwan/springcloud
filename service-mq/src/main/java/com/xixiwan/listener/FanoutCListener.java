package com.xixiwan.listener;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import com.xixiwan.config.FanoutConfig;

@Component
@RabbitListener(queues = FanoutConfig.FANOUT_QUEUE_C)
public class FanoutCListener {

	@RabbitHandler
	public void process(String message) {
		System.out.println("FanoutReceiverC  : " + message);
	}
}
