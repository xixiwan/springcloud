package com.xixiwan.listener;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import com.xixiwan.config.SimplestConfig;

@Component
@RabbitListener(queues = SimplestConfig.SIMPLEST_QUEUE)
public class SimplestListener {

	@RabbitHandler
	public void process(String message) {
		System.out.println("SimplestReceiver  : " + message);
	}
}
