package com.xixiwan.listener;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import com.xixiwan.config.DirectConfig;

@Component
@RabbitListener(queues = DirectConfig.DIRECT_QUEUE_B)
public class DirectBListener {

	@RabbitHandler
	public void process(String message) {
		System.out.println("DirectReceiverB  : " + message);
	}
}
