package com.xixiwan.listener;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import com.xixiwan.config.TopicConfig;

@Component
@RabbitListener(queues = TopicConfig.TOPIC_QUEUE_A)
public class TopicAListener {

	@RabbitHandler
	public void process(String message) {
		System.out.println("TopicReceiverA  : " + message);
	}
}
