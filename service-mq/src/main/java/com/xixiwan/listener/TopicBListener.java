package com.xixiwan.listener;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import com.xixiwan.config.TopicConfig;

@Component
@RabbitListener(queues = TopicConfig.TOPIC_QUEUE_B)
public class TopicBListener {

	@RabbitHandler
	public void process(String message) {
		System.out.println("TopicReceiverB  : " + message);
	}
}
