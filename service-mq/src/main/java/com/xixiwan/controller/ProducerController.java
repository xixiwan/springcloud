package com.xixiwan.controller;

import java.util.Date;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.xixiwan.config.DirectConfig;
import com.xixiwan.config.FanoutConfig;
import com.xixiwan.config.SimplestConfig;
import com.xixiwan.config.TopicConfig;

@RestController
public class ProducerController {

	@Autowired
	private AmqpTemplate amqpTemplate;

	@GetMapping("/simplest")
	@HystrixCommand(fallbackMethod = "simplestError")
	public String simplest(@RequestParam("name") String name) {
		String message = "hello " + name + " " + new Date();
		amqpTemplate.convertAndSend(SimplestConfig.SIMPLEST_QUEUE, message);
		return message;
	}

	public String simplestError(String name) {
		return "hi," + name + ",sorry,error!";
	}

	@GetMapping("/fanout")
	@HystrixCommand(fallbackMethod = "fanoutError")
	public String fanout(@RequestParam("name") String name) {
		String message = "hello " + name + " " + new Date();
		amqpTemplate.convertAndSend(FanoutConfig.FANOUT_EXCHANGE, "", message);
		return message;
	}

	public String fanoutError(String name) {
		return "hi," + name + ",sorry,error!";
	}

	@GetMapping("/direct/{routingKey}")
	@HystrixCommand(fallbackMethod = "directError")
	public String direct(@PathVariable("routingKey") String routingKey, @RequestParam("name") String name) {
		String message = "hello " + routingKey + " " + name + " " + new Date();
		amqpTemplate.convertAndSend(DirectConfig.DIRECT_EXCHANGE, routingKey, message);
		return message;
	}

	public String directError(String routingKey, String name) {
		return "hi," + routingKey + " " + name + ",sorry,error!";
	}

	@GetMapping("/topic/{routingKey}")
	@HystrixCommand(fallbackMethod = "topicError")
	public String topic(@PathVariable("routingKey") String routingKey, @RequestParam("name") String name) {
		String message = "hello " + routingKey + " " + name + " " + new Date();
		amqpTemplate.convertAndSend(TopicConfig.TOPIC_EXCHANGE, routingKey, message);
		return message;
	}

	public String topicError(String routingKey, String name) {
		return "hi," + routingKey + " " + name + ",sorry,error!";
	}
}
