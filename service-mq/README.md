# springcloud
# 访问地址
# http://localhost:8775/simplest?name=xixiwan
# http://localhost:8776/simplest?name=xixiwan
# http://localhost:8775/fanout?name=xixiwan
# http://localhost:8776/fanout?name=xixiwan
# http://localhost:8775/direct/{routingKey}?name=xixiwan
# http://localhost:8776/direct/{routingKey}?name=xixiwan
# http://localhost:8775/topic/{routingKey}?name=xixiwan
# http://localhost:8776/topic/{routingKey}?name=xixiwan
# http://localhost:8775/hystrix
# http://localhost:8775/hystrix.stream
# http://localhost:8776/hystrix
# http://localhost:8776/hystrix.stream

#Simplest 工作模式(竞争) 多个消费者不会重复消费，需要指定routing key
#Fanout 发布订阅模式(竞争) 多个消费者不会重复消费，不同消费者可以重复接收同一个交换机的消息，不需要指定routing key
#Direct 路由模式(竞争) 多个消费者不会重复消费，需要指定routing key
#Topic 主题模式(竞争) 多个消费者不会重复消费，需要指定routing key
#符号*：只能匹配一个词lazy.* 可以匹配lazy.irs或者lazy.cor
#符号#：匹配一个或者多个词lazy.# 可以匹配lazy.irs或者lazy.irs.cor